package com.nostratech.project.persistence.vo;

import lombok.Data;

import java.util.List;

@Data
public class ListOfMigrasiVO
{
    private List<MigrasiVO> migrasiVOList;
}