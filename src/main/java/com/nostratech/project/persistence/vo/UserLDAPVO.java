package com.nostratech.project.persistence.vo;

import lombok.Data;
import java.util.List;

@Data
public class UserLDAPVO
    implements Comparable<UserLDAPVO>
{
    private String uid;
    private String givenName;
    private String mail;
    private String cn;
    private String sn;
    private String orclActiveStartDate;
    private String orclIsEnabled;
    private String orclSAMAccountName;
    private String pwdpolicysubentry;
    private String facebookID;
    private String myTelkomID;
    private String portalToken;
    private String twitterID;
    private String ou;
    private String baseDN;
    private List<ObjectClassVO> objectClassVO;

    @Override
    public int compareTo(UserLDAPVO o)
    {
        if(getBaseDN().equalsIgnoreCase(o.getBaseDN())) return 1;
        else return 0;
    }
}