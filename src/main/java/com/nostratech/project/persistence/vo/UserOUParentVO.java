package com.nostratech.project.persistence.vo;


import lombok.Data;

import java.util.List;

@Data
public class UserOUParentVO {

    private String ouParent;
    private List<ObjectClassVO> objectClassVO;

}
