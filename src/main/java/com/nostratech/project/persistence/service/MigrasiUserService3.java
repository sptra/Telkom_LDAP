package com.nostratech.project.persistence.service;

import com.nostratech.project.persistence.vo.ListOfMigrasiVO;
import com.nostratech.project.persistence.vo.MigrasiVO;
import com.nostratech.project.util.LDAPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
/**
 * @author : Yohan
 * @date : 07 Mei 2018
 */
public class MigrasiUserService3
{
    public List<ListOfMigrasiVO> getAttributesAndBaseDN(long expected)
    {
        try
        {
            DirContext context = LDAPUtil.connectIntoLDAP(false);
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            searchControls.setCountLimit(expected);

            NamingEnumeration<SearchResult> getAllUsers = context.search("cn=Users,dc=telkom,dc=co,dc=id","(objectClass=*)", searchControls);
            List<String> allBaseDN = this.collectAllBaseDN(getAllUsers);

            return this.findAllAttributeAndValue(allBaseDN, context);
        }

        catch(Exception e)
        {
            log.info("Error getAttributesAndBaseDN, cause {}",e.getCause());
            e.printStackTrace();
        }

        return null;
    }

    public Boolean insertUser(long expectedRows)
    {
        try
        {
            log.info("ENTER LINE 49");
            List<ListOfMigrasiVO> migrasiVOS = this.getAttributesAndBaseDN(expectedRows);
            DirContext context = LDAPUtil.connectIntoLDAP(true);
            long flag = 0; int iterasi = 0;

            for(ListOfMigrasiVO mivo : migrasiVOS)
            {
                log.info("ITERASI ------> [{}]",iterasi);
                List<String> valKey = new ArrayList<>();
                List<String> valAtr = new ArrayList<>();
                List<String> valBDN = new ArrayList<>();

                for(MigrasiVO migrasiVO : mivo.getMigrasiVOList())
                {
                    if(!migrasiVO.getStrKey().toLowerCase().contains("authpassword"))
                    {
                        valKey.add(migrasiVO.getStrKey());
                        valAtr.add(migrasiVO.getStrValue());
                        valBDN.add(migrasiVO.getBaseDN());
                    }
                }

                BasicAttributes basicAttribute = new BasicAttributes();
                Attribute attribute = new BasicAttribute("objectClass");
                String baseDN = null;

                for(int i=0; i<valKey.size(); i++)
                {
                    if(valKey.get(i).toLowerCase().contains("objectclass")){
                        attribute.add(valAtr.get(i));
                    } else {
                        basicAttribute.put(valKey.get(i), valAtr.get(i));
                    }

                    basicAttribute.put(attribute);
                    baseDN = valBDN.get(i);
                }

                int lengthBaseDN = baseDN.split(",").length;
                SearchControls controls = new SearchControls();
                controls.setSearchScope(SearchControls.SUBTREE_SCOPE);

//                ABCDE
                if(lengthBaseDN == 5)
                {
                    String filter1 = baseDN.split(",")[0];
                    String baseDNFilter1 = "";

                    for(int i=1; i<lengthBaseDN; i++) {
                        baseDNFilter1 += baseDN.split(",")[i] + ",";
                    }

                    baseDNFilter1 = baseDNFilter1.substring(0,baseDNFilter1.length()-1);
                    NamingEnumeration<SearchResult> sr = context.search(baseDNFilter1, filter1 , controls);

                    if(sr.hasMoreElements() == false)
                    {
                        context.createSubcontext("ldap://10.0.63.96:3060/"+baseDN, basicAttribute);
                        flag += 1;
                        log.info("Insert BASEDN 5 --> BaseDN : [{}]", baseDN);
                    }
                }

//                ABCDEF
                else if(lengthBaseDN == 6)
                {
                    String filter1 = baseDN.split(",")[1];
                    String baseDNFilter1 = "";

                    for(int i=2; i<lengthBaseDN; i++) {
                        baseDNFilter1 += baseDN.split(",")[i] + ",";
                    }

                    baseDNFilter1 = baseDNFilter1.substring(0,baseDNFilter1.length()-1);
                    NamingEnumeration<SearchResult> sr = context.search(baseDNFilter1, filter1 , controls);

                    if(sr.hasMoreElements())
                    {
                        
                    }

                    else
                    {

                    }
                }

                else if(lengthBaseDN == 7)
                {
                    NamingEnumeration<SearchResult> sr = context.search(baseDN, "(objectclass=*)", controls);
                    if(sr.hasMoreElements() == false)
                    {
                        context.createSubcontext("ldap://10.0.63.96:3060/"+baseDN, basicAttribute);
                        flag += 1;
                        log.info("Insert BASEDN 6 --> BaseDN : [{}]", baseDN);
                    }
                }

                iterasi += 1;
            }

            log.info("Flag = [{}], MigrasiVO = [{}]", flag, migrasiVOS.size());

            return (flag > 0);
        }

        catch(Exception e)
        {
            log.info("Error insertUser, cause {}",e.getCause());
            e.printStackTrace();
        }

        return false;
    }

    private List<String> collectAllBaseDN(NamingEnumeration<SearchResult> neGetAllUsers)
    {
        try
        {
            List<String> result = new ArrayList<>();

            while(neGetAllUsers.hasMoreElements()){
                result.add(neGetAllUsers.next().getNameInNamespace());
            }

            return result;
        }

        catch(Exception e)
        {
            log.info("Error collectAllBaseDN, cause {}",e.getCause());
            e.printStackTrace();
        }

        return null;
    }

    private List<ListOfMigrasiVO> findAllAttributeAndValue(List<String> fullBaseDN, DirContext context)
    {
        try
        {
            List<ListOfMigrasiVO> result = new ArrayList<>();

            for(String str : fullBaseDN)
            {
                List<MigrasiVO> mvo1 = new ArrayList<>();
                ListOfMigrasiVO migrasiVO1 = new ListOfMigrasiVO();
                Attributes findAttribute = context.getAttributes(str);

                for(NamingEnumeration ne1 = findAttribute.getAll(); ne1.hasMoreElements();)
                {
                    Attribute atr = (Attribute) ne1.next();
                    for(NamingEnumeration ne2 = atr.getAll(); ne2.hasMoreElements();) {
                        mvo1.add(new MigrasiVO(atr.getID(), ne2.next().toString(), str));
                    }
                }

                migrasiVO1.setMigrasiVOList(mvo1);
                result.add(migrasiVO1);
            }

            return result;
        }

        catch(Exception e)
        {
            log.info("Error findAllAttributeAndValue, cause {}",e.getCause());
            e.printStackTrace();
        }

        return null;
    }
}