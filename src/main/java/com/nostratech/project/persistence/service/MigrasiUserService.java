package com.nostratech.project.persistence.service;

import com.nostratech.project.persistence.vo.JumlahBaseDNVO;
import com.nostratech.project.persistence.vo.ObjectClassVO;
import com.nostratech.project.persistence.vo.UserLDAPVO;
import com.nostratech.project.util.LDAPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import static java.util.concurrent.Executors.*;

@Service
@Slf4j
/**
 * @author : Yohan
 */
public class MigrasiUserService
{
    public List<String> getAllBaseDN(boolean paramPRDSentul, long expectedJumlah)
    {
        try
        {
            DirContext context = LDAPUtil.connectIntoLDAP(paramPRDSentul);

            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            NamingEnumeration<SearchResult> ne = context.search("cn=Users,dc=telkom,dc=co,dc=id", "(objectClass=*)", controls);
            List<String> result = new ArrayList<>();

            while(ne.hasMoreElements() == true)
            {
                SearchResult sr =  ne.next();
                result.add(sr.getNameInNamespace());
            }

            log.info("Jumlah BaseDN = ["+result.size()+"]");

            return result;
        }

        catch(Exception e)
        {
            log.info("Error couldn't get all base DN LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public List<JumlahBaseDNVO> countBaseDN()
    {
        try
        {
            List<UserLDAPVO> listODS = this.getAllUserAndAttributeAndObjectClassLDAP(false, 1000000, LDAPUtil.connectIntoLDAP(false));
            List<JumlahBaseDNVO> result = new ArrayList<>();

            Integer dn7 = 0, dn8 = 0, dn9 = 0, dn10 = 0, dn11 = 0, dn12 = 0;

            for(int i = 0; i < listODS.size();  i++)
            {
                String baseDN = listODS.get(i).getBaseDN();
                int jumlahBaseDN = baseDN.split(",").length;
                JumlahBaseDNVO jumlahBaseDNVO = new JumlahBaseDNVO();

                if(jumlahBaseDN == 7)
                {
                    jumlahBaseDNVO.setBaseDN7(listODS.get(i).getBaseDN());
                    dn7++;
                }

                if(jumlahBaseDN == 8)
                {
                    jumlahBaseDNVO.setBaseDN8(listODS.get(i).getBaseDN());
                    dn8++;
                }

                if(jumlahBaseDN == 9)
                {
                    jumlahBaseDNVO.setBaseDN9(listODS.get(i).getBaseDN());
                    dn9++;
                }

                if(jumlahBaseDN == 10)
                {
                    jumlahBaseDNVO.setBaseDN10(listODS.get(i).getBaseDN());
                    dn10++;
                }

                if(jumlahBaseDN == 11)
                {
                    jumlahBaseDNVO.setBaseDN11(listODS.get(i).getBaseDN());
                    dn11++;
                }

                if(jumlahBaseDN == 12)
                {
                    jumlahBaseDNVO.setBaseDN12(listODS.get(i).getBaseDN());
                    dn12++;
                }

                result.add(jumlahBaseDNVO);
            }

            log.info("Jumlah 7 = ["+dn7+"]");
            log.info("Jumlah 8 = ["+dn8+"]");
            log.info("Jumlah 9 = ["+dn9+"]");
            log.info("Jumlah 10 = ["+dn10+"]");
            log.info("Jumlah 11 = ["+dn11+"]");
            log.info("Jumlah 12 = ["+dn12+"]");

            return result;
        }

        catch(Exception e)
        {
            log.info("Error couldn't get count baseDN, cause "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public List<UserLDAPVO> getAllUserAndAttributeAndObjectClassLDAP(boolean paramPRDSentul, long expectedJumlah, DirContext paramContext)
    {
        ExecutorService es = newFixedThreadPool(5000);
        List<UserLDAPVO> userLDAPVOList = new ArrayList<UserLDAPVO>();

        es.execute(() ->
        {
            try
            {
                log.info("Expected = [{}]",expectedJumlah);

                DirContext contextODS = null; Integer x = 0;

                if(paramContext == null) contextODS = LDAPUtil.connectIntoLDAP(paramPRDSentul);

                SearchControls controls = new SearchControls();
                controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
                controls.setCountLimit(expectedJumlah);

                controls.setReturningAttributes(new String[]
                        {
                                "uid", "givenname", "mail", "cn", "sn",
                                "objectClass", "orclActiveStartDate", "orclIsEnabled", "orclSAMAccountName", "pwdpolicysubentry",
                                "facebookID", "myTelkomID", "portaltoken", "twitterID", "ou"
                        });

                NamingEnumeration<SearchResult> findAllUser = contextODS.search("cn=Users,dc=telkom,dc=co,dc=id", "(objectClass=*)", controls);
                log.info("=====Fetching Data, Please wait=====");

                while(findAllUser.hasMoreElements() == true)
                {
                    SearchResult sr = findAllUser.next();

                    Attributes atts = sr.getAttributes();
                    UserLDAPVO ldapvo = new UserLDAPVO();
                    List<ObjectClassVO> objectClassVOList = new ArrayList<>();

                    Attribute atr = atts.get("objectClass");
                    for(NamingEnumeration ne1 = atr.getAll(); ne1.hasMoreElements();) objectClassVOList.add(new ObjectClassVO("" + ne1.next()));

                    if(atts.get("uid") == null)
                        ldapvo.setUid(null);
                    else
                        ldapvo.setUid(atts.get("uid").get().toString());

                    if(atts.get("givenname") == null)
                        ldapvo.setGivenName(null);
                    else
                        ldapvo.setGivenName(atts.get("givenname").get().toString());

                    if(atts.get("mail") == null)
                        ldapvo.setMail(null);
                    else
                        ldapvo.setMail(atts.get("mail").get().toString());

                    if(atts.get("cn") == null)
                        ldapvo.setCn(null);
                    else
                        ldapvo.setCn(atts.get("cn").get().toString());

                    if(atts.get("sn") == null)
                        ldapvo.setSn(null);
                    else
                        ldapvo.setSn(atts.get("sn").get().toString());

                    if(atts.get("orclActiveStartDate") == null)
                        ldapvo.setOrclActiveStartDate(null);
                    else
                        ldapvo.setOrclActiveStartDate(atts.get("orclActiveStartDate").get().toString());

                    if(atts.get("orclIsEnabled") == null)
                        ldapvo.setOrclIsEnabled(null);
                    else
                        ldapvo.setOrclIsEnabled(atts.get("orclIsEnabled").get().toString());

                    if(atts.get("orclSAMAccountName") == null)
                        ldapvo.setOrclSAMAccountName(null);
                    else
                        ldapvo.setOrclSAMAccountName(atts.get("orclSAMAccountName").get().toString());

                    if(atts.get("pwdpolicysubentry") == null)
                        ldapvo.setPwdpolicysubentry(null);
                    else
                        ldapvo.setPwdpolicysubentry(atts.get("pwdpolicysubentry").get().toString());

                    if(atts.get("facebookID") == null)
                        ldapvo.setFacebookID(null);
                    else
                        ldapvo.setFacebookID(atts.get("facebookID").get().toString().trim());

                    if(atts.get("myTelkomID") == null)
                        ldapvo.setMyTelkomID(null);
                    else
                        ldapvo.setMyTelkomID(atts.get("myTelkomID").get().toString().trim());

                    if(atts.get("portaltoken") == null)
                        ldapvo.setPortalToken(null);
                    else
                        ldapvo.setPortalToken(atts.get("portaltoken").get().toString().trim());

                    if(atts.get("twitterID") == null)
                        ldapvo.setTwitterID(null);
                    else
                        ldapvo.setTwitterID(atts.get("twitterID").get().toString().trim());

                    if(atts.get("ou") == null)
                        ldapvo.setOu(null);
                    else
                        ldapvo.setOu(atts.get("ou").get().toString());

                    ldapvo.setBaseDN(sr.getNameInNamespace());
                    ldapvo.setObjectClassVO(objectClassVOList);
                    userLDAPVOList.add(ldapvo);

                    log.info("Loop [{}]",x);

                    x++;
                }

                log.info("=====Finish Fetch All Data=====");
                log.info("Total All ObjectClass = ["+userLDAPVOList.size()+"]");
                log.info("=====Proses Write Responses into CSV======");

                String pathSaveCSV = "/Users/yohan/Documents/expOIDJava/expt.txt";
                FileWriter fw = new FileWriter(pathSaveCSV);
                BufferedWriter br = new BufferedWriter(fw);

                for(UserLDAPVO ldapvo : userLDAPVOList)
                {
                    String str = "";
                    for(int i=0; i<ldapvo.getObjectClassVO().size(); i++)
                        str += "objectclass: "+ldapvo.getObjectClassVO().get(i).getObjClass()+"\n";
                    str = str.trim();

                    String cn = "";
                    if(ldapvo.getCn() != null) cn = "cn: "+ldapvo.getCn();

                    String dn = "";
                    if(ldapvo.getBaseDN() != null) dn = "dn: "+ldapvo.getBaseDN();

                    String fbid = "";
                    if(ldapvo.getFacebookID() != null) fbid = "facebookID: "+ldapvo.getFacebookID();

                    String uid = "";
                    if(ldapvo.getUid() != null) uid = "uid: "+ldapvo.getUid();

                    String givename = "";
                    if(ldapvo.getGivenName() != null) givename = "givenName: "+ldapvo.getGivenName();

                    String mail = "";
                    if(ldapvo.getMail() != null) mail = "mail: "+ldapvo.getMail();

                    String sn = "";
                    if(ldapvo.getSn() != null) sn = "sn: "+ldapvo.getSn();

                    String orclActiveStartDate = "";
                    if(ldapvo.getOrclActiveStartDate() != null) orclActiveStartDate = "orclActiveStartDate: "+ldapvo.getOrclActiveStartDate();

                    String orclIsEnabled = "";
                    if(ldapvo.getOrclIsEnabled() != null) orclIsEnabled = "orclIsEnabled: " +ldapvo.getOrclIsEnabled();

                    String orclSAMAccountName = "";
                    if(ldapvo.getOrclSAMAccountName() != null) orclSAMAccountName = "orclSAMAccountName: "+ldapvo.getOrclSAMAccountName();

                    String pwdPolicySubentry = "";
                    if(ldapvo.getPwdpolicysubentry() != null) pwdPolicySubentry = "pwdPolicySubentry: "+ldapvo.getPwdpolicysubentry();

                    String myTelkomID = "";
                    if(ldapvo.getMyTelkomID() != null) myTelkomID = "myTelkomID: "+ldapvo.getMyTelkomID();

                    String portaltoken = "";
                    if(ldapvo.getPortalToken() !=null) portaltoken = "portaltoken: "+ldapvo.getPortalToken();

                    String twitterID = "";
                    if(ldapvo.getTwitterID() != null) twitterID = "twitterID: "+ldapvo.getTwitterID();

                    String ou = "";
                    if(ldapvo.getOu() != null) ou = "ou: "+ldapvo.getOu();

                    br.write
                    (
                    dn.trim() + "\n" +
                        cn.trim() + "\n" +
                        str +
                        ou.trim() + "\n" +
                        uid.trim() + "\n" +
                        givename.trim() + "\n" +
                        mail.trim() + "\n" +
                        sn.trim() + "\n" +
                        orclActiveStartDate.trim() +"\n" +
                        orclIsEnabled.trim() +"\n"+
                        orclSAMAccountName.trim() + "\n" +
                        pwdPolicySubentry.trim() + "\n" +
                        myTelkomID.trim() + "\n" +
                        portaltoken.trim() + "\n" +
                        twitterID.trim() + "\n" +
                        fbid.trim() + "\n" +
                        " Q==" + "\n\n"
                    );
                }
                log.info("=====Selesai Write Responses into CSV======");
                br.close(); fw.close();
            }

            catch(Exception e)
            {
                log.info("Error couldn't get all user LDAP, cause "+e.getMessage());
                e.printStackTrace();
            }
        });

        es.shutdown();

        return Arrays.asList(new UserLDAPVO());
    }

    public Boolean insertUser(long expectedJumlah)
    {
        try
        {
            Integer baseDN5 = 0, baseDN6 = 0, baseDN7 = 0, baseDN8 = 0, baseDN9 = 0, baseDN10 = 0, baseDN11 = 0, baseDN12 = 0;
            DirContext contextPRDSentul = LDAPUtil.connectIntoLDAP(true);
            List<UserLDAPVO> userLDAPVOList = this.getAllUserAndAttributeAndObjectClassLDAP(false, expectedJumlah, null);

            for(int i=0; i<userLDAPVOList.size(); i++)
            {
                int jumBaseDN = userLDAPVOList.get(i).getBaseDN().split(",").length;

                if(jumBaseDN == 5) baseDN5++;
                else if(jumBaseDN == 6) baseDN6++;
                else if(jumBaseDN == 7) baseDN7++;
                else if(jumBaseDN == 8) baseDN8++;
                else if(jumBaseDN == 9) baseDN9++;
                else if(jumBaseDN == 10) baseDN10++;
                else if(jumBaseDN == 11) baseDN11++;
                else if(jumBaseDN == 12) baseDN12++;

                if(jumBaseDN == 5)
                {
                    if(checkParent(contextPRDSentul, userLDAPVOList.get(i).getBaseDN(), 5) == 50) //kedalaman 5 tidak ada
                    {
//                      insert untuk kedalaman ke 5
                        String fullBaseDN = "", filter = "";
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }
                }

                else if(jumBaseDN == 6)
                {
                    Integer resultCheckParent = checkParent(contextPRDSentul, userLDAPVOList.get(i).getBaseDN(), 6);
                    String fullBaseDN = "", filter = "";

                    if(resultCheckParent == 61) //kedalaman 5 ada, kedalaman 6 tidak ada
                    {
//                        insert untuk kedalaman ke 6
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }

                    else if(resultCheckParent == 60) //kedalaman 5 tidak
                    {
//                      insert untuk kedalaman ke 5
                        for(int j=2; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[1]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);

                        fullBaseDN = ""; filter = "";
//                        insert untuk kedalaman ke 6
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }
                }

                else if(jumBaseDN == 7)
                {
                    Integer resultCheckParent = checkParent(contextPRDSentul, userLDAPVOList.get(i).getBaseDN(), 7);
                    String fullBaseDN = "", filter = "";

                    if(resultCheckParent == 7) //kedalaman ke 7 tidak ada, kedalaman 5-6 ada
                    {
//                        insert untuk kedalaman ke 7
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }

                    else if(resultCheckParent == 71) //kedalaman 5 ada, tapi kedalaman 6-7 tidak ada
                    {
//                        insert untuk kedalaman ke 6
                        for(int j=2; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[1]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);

                        fullBaseDN = ""; filter = "";
//                        insert untuk kedalaman ke 7
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }

                    else if(resultCheckParent == 70) //kedalaman 5 tidak ada
                    {
//                        insert untuk kedalaman ke 5
                        for(int j=3; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[2]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);

                        fullBaseDN = ""; filter = "";
//                        insert untuk kedalaman ke 6
                        for(int j=2; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[1]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);

                        fullBaseDN = ""; filter = "";
//                        insert untuk kedalaman ke 7
                        for(int j=1; j<jumBaseDN; j++) fullBaseDN += userLDAPVOList.get(i).getBaseDN().split(",")[j] + ",";
                        fullBaseDN = fullBaseDN.substring(0,fullBaseDN.length()-1);
                        filter = "("+userLDAPVOList.get(i).getBaseDN().split(",")[0]+")";
                        this.insertUser(contextPRDSentul, userLDAPVOList, i, expectedJumlah, fullBaseDN, filter);
                    }
                }
            }

            log.info("Jumlah BaseDN5 = [{}], BaseDN6 = [{}], BaseDN7 = [{}], BaseDN8 = [{}], BaseDN9 = [{}], BaseDN10 = [{}], BaseDN11 = [{}], BaseDN12 = [{}]",
                    baseDN5, baseDN6, baseDN7, baseDN8, baseDN9, baseDN10, baseDN11, baseDN12);

            return true;
        }

        catch(Exception e)
        {
            log.info("Error couldn't InsertUser into LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    private Integer checkParent(DirContext ctx, String fullBaseDN, int pattern)
    {
        try
        {
            SearchControls sc = new SearchControls();
            sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

            if(pattern == 5)
            {
                String baseDN = "", filter = "("+fullBaseDN.split(",")[0]+")";
                for(int i=1; i<fullBaseDN.split(",").length; i++) baseDN += fullBaseDN.split(",")[i] + ",";
                baseDN = baseDN.substring(0, baseDN.length()-1);

                NamingEnumeration<SearchResult> sr = ctx.search(baseDN, filter, sc);
                if(sr.hasMoreElements() == true)
                    return 5;
                else
                    return 50;
            }

            else if(pattern == 6)
            {
                String baseDN1 = "", filter1 = "("+fullBaseDN.split(",")[1]+")";
                for(int i=2; i<fullBaseDN.split(",").length; i++) baseDN1 += fullBaseDN.split(",")[i] + ",";
                baseDN1 = baseDN1.substring(0, baseDN1.length()-1);

                String baseDN2 = "", filter2 = "("+fullBaseDN.split(",")[0]+")";
                for(int i=1; i<fullBaseDN.split(",").length; i++) baseDN2 += fullBaseDN.split(",")[i] + ",";
                baseDN2 = baseDN2.substring(0, baseDN2.length()-1);

                NamingEnumeration<SearchResult> sr = ctx.search(baseDN1, filter1, sc);

                if(sr.hasMoreElements() == true)
                {
                    NamingEnumeration<SearchResult> sr2 = ctx.search(baseDN2, filter2, sc);
                    if(sr2.hasMoreElements() == true)
                        return 6;
                    else
                        return 61;
                }
                else
                    return 60;
            }

            else if(pattern == 7)
            {
                String baseDN1 = "", filter1 = "("+fullBaseDN.split(",")[2]+")";
                for(int i=3; i<fullBaseDN.split(",").length; i++) baseDN1 += fullBaseDN.split(",")[i] + ",";
                baseDN1 = baseDN1.substring(0, baseDN1.length()-1);

                NamingEnumeration<SearchResult> sr = ctx.search(baseDN1, filter1, sc);

                if(sr.hasMoreElements() == true)
                {
                    String baseDN2 = "", filter2 = "("+fullBaseDN.split(",")[1]+")";
                    for(int i=2; i<fullBaseDN.split(",").length; i++) baseDN2 += fullBaseDN.split(",")[i] + ",";
                    baseDN2 = baseDN2.substring(0, baseDN2.length()-1);

                    log.info("BaseDN7 = [{}], Filter = [{}]", baseDN2, filter2);

                    String baseDN3 = "", filter3 = "("+fullBaseDN.split(",")[0]+")";
                    for(int i=1; i<fullBaseDN.split(",").length; i++) baseDN3 += fullBaseDN.split(",")[i] + ",";
                    baseDN3 = baseDN3.substring(0, baseDN3.length()-1);

                    NamingEnumeration<SearchResult> sr2 = ctx.search(baseDN2, filter2, sc);
                    NamingEnumeration<SearchResult> sr3 = ctx.search(baseDN3, filter3, sc);
                    if(sr2.hasMoreElements() == true && sr3.hasMoreElements() == false)
                        return 7;
                    else
                        return 71;
                }

                else
                    return 70;
            }
        }

        catch(Exception e)
        {
            log.info("Error couldn't checkParent into LDAP, cause "+e.getMessage());
            e.printStackTrace();
        }

        return 0;
    }

    private Boolean insertUser(DirContext ctx, List<UserLDAPVO> listODS, int posisi, long jumlahExcepted, String concat, String filter)
    {
        try
        {
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setCountLimit(jumlahExcepted);

            NamingEnumeration<SearchResult> sr = ctx.search(concat, filter, controls);

            if(sr.hasMoreElements() == false)
            {
                BasicAttributes basicAttribute = new BasicAttributes();
                Attribute attribute = new BasicAttribute("objectClass");

                int jumlahObjectVO = listODS.get(posisi).getObjectClassVO().size();
                for(int j=0; j<jumlahObjectVO; j++) attribute.add(listODS.get(posisi).getObjectClassVO().get(j).getObjClass());

                basicAttribute.put(attribute);

                if(listODS.get(posisi).getUid() != null && !listODS.get(posisi).getUid().equalsIgnoreCase("null")) basicAttribute.put("uid", listODS.get(posisi).getUid());
                if(listODS.get(posisi).getGivenName() != null && !listODS.get(posisi).getGivenName().equalsIgnoreCase("null")) basicAttribute.put("givenname", listODS.get(posisi).getGivenName());
                if(listODS.get(posisi).getMail() != null && !listODS.get(posisi).getMail().equalsIgnoreCase("null")) basicAttribute.put("mail", listODS.get(posisi).getMail());
                if(listODS.get(posisi).getCn() != null && !listODS.get(posisi).getCn().equalsIgnoreCase("null")) basicAttribute.put("cn", listODS.get(posisi).getCn());
                if(listODS.get(posisi).getSn() != null && !listODS.get(posisi).getSn().equalsIgnoreCase("null")) basicAttribute.put("sn", listODS.get(posisi).getSn());
                if(listODS.get(posisi).getOrclActiveStartDate() != null && !listODS.get(posisi).getOrclActiveStartDate().equalsIgnoreCase("null")) basicAttribute.put("orclActiveStartDate", listODS.get(posisi).getOrclActiveStartDate());
                if(listODS.get(posisi).getOrclIsEnabled() != null && !listODS.get(posisi).getOrclIsEnabled().equalsIgnoreCase("null")) basicAttribute.put("orclIsEnabled", listODS.get(posisi).getOrclIsEnabled());
                if(listODS.get(posisi).getOrclSAMAccountName() != null && !listODS.get(posisi).getOrclSAMAccountName().equalsIgnoreCase("null")) basicAttribute.put("orclSAMAccountName", listODS.get(posisi).getOrclSAMAccountName());
                if(listODS.get(posisi).getPwdpolicysubentry()  != null && !listODS.get(posisi).getPwdpolicysubentry().equalsIgnoreCase("null")) basicAttribute.put("pwdpolicysubentry", listODS.get(posisi).getPwdpolicysubentry());
                if(listODS.get(posisi).getOu() != null && !listODS.get(posisi).getOu().equalsIgnoreCase("null")) basicAttribute.put("ou", listODS.get(posisi).getOu());

                if(listODS.get(posisi).getFacebookID() != null && !listODS.get(posisi).getFacebookID().equalsIgnoreCase("null")) basicAttribute.put("facebookID", listODS.get(posisi).getFacebookID());
                if(listODS.get(posisi).getMyTelkomID() != null && !listODS.get(posisi).getMyTelkomID().equalsIgnoreCase("null")) basicAttribute.put("myTelkomID", listODS.get(posisi).getMyTelkomID());
                if(listODS.get(posisi).getPortalToken() != null && !listODS.get(posisi).getPortalToken().equalsIgnoreCase("null")) basicAttribute.put("portaltoken", listODS.get(posisi).getPortalToken());
                if(listODS.get(posisi).getTwitterID() != null && !listODS.get(posisi).getTwitterID().equalsIgnoreCase("null")) basicAttribute.put("twitterID", listODS.get(posisi).getTwitterID());

                String parseBaseDN = filter.substring(1,filter.length()-1)+","+concat;
                ctx.createSubcontext("ldap://10.0.63.97:3060/"+parseBaseDN, basicAttribute);
                log.info("New User Details : BaseDB [{}], Filter = [{}]\tStatus : Berhasil Ditambahkan", parseBaseDN, filter);

                return true;
            }
        }

        catch(Exception e)
        {
            log.info("Error couldn't Insert User 5to6 Detail, cause "+e.getMessage());
            e.printStackTrace();
        }

        return false;
    }

    public List<UserLDAPVO> distinctBaseDN(long jumlah)
    {
        try
        {
            List<UserLDAPVO> ldapvoList = this.getAllUserAndAttributeAndObjectClassLDAP(false, jumlah, null);
            log.info("Size Before Distinct = [{}]", ldapvoList.size());

            ldapvoList.parallelStream().filter(distinctByKey(p -> p.getBaseDN())).collect(Collectors.toList());
            log.info("Size After Distinct = [{}]", ldapvoList.size());

            return ldapvoList;
        }

        catch(Exception e)
        {
            log.info("Error couldn't Distinct baseDN, cause "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public Set<UserLDAPVO> readFromCSV()
    {
        try(BufferedReader nBR = new BufferedReader(new FileReader("/Users/yohan/Documents/NitipYohan/ODSEdited.txt")))
        {
            String curLine;
            Set<UserLDAPVO> userLDAPVOS = new HashSet<>();

            log.info("=====Start Fetching From TXT=====");

            while((curLine = nBR.readLine())!=null)
            {
                UserLDAPVO ldapvo = new UserLDAPVO();
                List<ObjectClassVO> objectClassVOS = new ArrayList<>();

                String str = curLine.split(";")[15];
                String[] str1 = str.substring(2,str.length()-2).split(",");

                for(int i=0; i<str1.length; i++)
                {
                    String[] k = str1[i].split("=");
                    for(int j=0; j<k.length; j++)
                    {
                        String panjang = k[j];
                        if(j%2==1)
                        {
                            objectClassVOS.add(new ObjectClassVO(panjang.substring(0,panjang.length()-1)));
                        }
                    }
                }

                ldapvo.setUid(curLine.split(";")[0]);
                ldapvo.setGivenName(curLine.split(";")[1]);
                ldapvo.setMail(curLine.split(";")[2]);
                ldapvo.setCn(curLine.split(";")[3]);
                ldapvo.setSn(curLine.split(";")[4]);
                ldapvo.setOrclActiveStartDate(curLine.split(";")[5]);
                ldapvo.setOrclIsEnabled(curLine.split(";")[6]);
                ldapvo.setOrclSAMAccountName(curLine.split(";")[7]);
                ldapvo.setPwdpolicysubentry(curLine.split(";")[8]);
                ldapvo.setFacebookID(curLine.split(";")[9]);
                ldapvo.setMyTelkomID(curLine.split(";")[10]);
                ldapvo.setPortalToken(curLine.split(";")[11]);
                ldapvo.setTwitterID(curLine.split(";")[12]);
                ldapvo.setOu(curLine.split(";")[13]);
                ldapvo.setBaseDN(curLine.split(";")[14]);
                ldapvo.setObjectClassVO(objectClassVOS);

                userLDAPVOS.add(ldapvo);

                log.info("CurLine = {}", curLine);
            }

            log.info("=====Finish Fetching From TXT=====");
            log.info("Jumlah Data = [{}]",userLDAPVOS.size());

            return userLDAPVOS;
        }

        catch(Exception e)
        {
            log.info("Error can't read file text, cause {}",e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    <T> Predicate<T> distinctByKey(Function<? super T, ?> ke)
    {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(ke.apply(t), Boolean.TRUE) == null;
    }
}